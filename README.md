**Premise of this script**

This is a bash script for easily opening Carla-Rack with pipewire's implementation of JACK

---

**Installing the script**

There are two main ways to download this script

> 1. Open a terminal window and git clone this repository

> 2. Open the script as raw, Right click, and save as

---

**Using the script**

> 1. Open a terminal in the same directory as the script and run chmod +x pw-carla.sh

---

**License Choice**

> I chose The 2-Clause BSD License due to the unrestrictive nature of it to allow others to easily fork and continute the project